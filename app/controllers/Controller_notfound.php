<?php


use Kernel\Controller;

class Controller_notfound extends Controller
{
    function action_main(array $post_args = null, string $get_args = null)
    {
        $this->view->generate('view_404.php',   'template_page.php');
    }
}