<?php

use Kernel\View;
use Kernel\Model;
use Kernel\Controller;

class Controller_find extends Controller
{
    public function action_main(array $post_args = null, string $get_args = null)
    {
        $data = $this->model->get_data($post_args, $get_args);
        $this->view->generate('view_find.php', 'template_json.php', $data);
    }

}