<?php

use Kernel\Controller;
class Controller_results extends Controller
{
    function action_main(array $post_args = null, string $get_args = null)
    {
        $data = $this->model->get_data($post_args, $get_args);
        $this->view->generate('view_results.php', 'template_page.php', $data);
    }
}