<?php

use Kernel\Controller;
use Kernel\View;

class Controller_main extends Controller
{
    public function action_main(array $post_args = null, string $get_args = null)
    {
        $this->view->generate('view_main.php', 'template_page.php');
    }

}
