<?php


namespace Kernel;

use PDO;
use PDOStatement;

class DBConnector
{
    private const DB_HOST = "localhost";
    private const DB_NAME = "findertask";
	private const DB_USER = "root";
	private const DB_PASS = "";

    private function __construct (){}
    private function __clone () {}
    private function __wakeup () {}

    public static function query(string $query)
    {
        $pdo = new PDO(
            'mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME,
            self::DB_USER,
            self::DB_PASS,
        );
//        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $result = $pdo->prepare($query);
        $result->execute();
        if ($result->rowCount() > 0)
            return $result;
        else
            return false;
    }

}