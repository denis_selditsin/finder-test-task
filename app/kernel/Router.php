<?php

namespace Kernel;


class Router
{
    private static string $model;
    private static string $controller = "main";
    private static string $action = "main";
    private static array $post_args = [];
    private static string $get_args = "";

    public static function route(): void
    {
        $uri = explode('?', $_SERVER['REQUEST_URI'])[0];
        $uri = explode('/', $uri);

        if(!empty($uri[1]))
            self::$controller = $uri[1];

        if(!empty($uri[2]))
            self::$action = $uri[2];
        
        if(!empty($uri[3]))
            self::$get_args = $uri[3];

        if(!empty($_POST))
            self::$post_args = $_POST;

        self::$model = 'Model_'.self::$controller;
        self::$controller = 'Controller_'.self::$controller;
        self::$action = 'action_'.self::$action;

        $model_path = "app/models/".self::$model.".php";
        if(file_exists($model_path))
            require_once $model_path;

        $controller_path = "app/controllers/".self::$controller.".php";
        if(file_exists($controller_path))
            require_once $controller_path;
        else
            Router::route_404();

        $controller = new self::$controller();
        $action = self::$action;
        if(method_exists($controller, $action))
            $controller->$action(self::$post_args, self::$get_args);
        else
            Router::route_404();
    }

    private static function route_404(): void
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'notfound');
    }

}