<?php


namespace Kernel;


class View
{
    //public View $template_view; // здесь можно указать общий вид по умолчанию.

    public function generate($content_view, $template_view, $data = null)
    {
        /*
        if(is_array($data)) {
            // преобразуем элементы массива в переменные
            extract($data);
        }
        */

        require_once "app/views/".$template_view;
    }

}