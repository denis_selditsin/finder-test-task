<?php

use Kernel\DBConnector;
use Kernel\Model;

class Model_results implements Model
{
    public function get_data(array $post_args = null, string $get_args = null)
    {
        $query = DBConnector::query("SELECT * FROM found_elements");
        if($query)
            return $query->fetchAll();
        else return false;
    }
}