<?php

use Kernel\DBConnector;
use Kernel\Model;

class Model_find implements Model
{
    private array $invalid;
    private string $url;
    private int $type;
    private string $text;
    private string $page;
    public function get_data(array $post_args = null, string $get_args = null):array
    {
        if($this->validate_form()){
            $this->get_page();
            $matches = [];
            if ($this->type === 0)
                $matches = $this->find_links();
            elseif ($this->type === 1)
                $matches = $this->find_images();
            elseif ($this->type === 2)
                $matches = $this->find_text();

            if(!empty($matches)) {
                if ($this->add_to_db($matches) == false) {
                    $this->invalid["db"] = "invalid";
                    return array("result" => "error", "invalid" => $this->invalid);
                }
            }
            else{
                $this->invalid["elements"] = "empty";
                return array("result" => "error", "invalid" => $this->invalid);
            }
            return array("result" => "ok", "answer" => "success");
        }else{
            return array("result" => "error", "invalid" => $this->invalid);
        }

    }

    private function add_to_db(array $matches)
    {
        $elements = "";
        foreach ($matches as $match){
            $elements .= $match."\r\n";
        }

        return DBConnector::query("INSERT INTO found_elements (url, elements, count) 
                                    VALUES ('".$this->url."', '".addslashes($elements)."', '".count($matches)."')");
    }

    private function get_page(){
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $this->page = curl_exec($ch);
        $ctype = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        $charset = null;
        if (($ctype !== null && preg_match('#charset=([\w-]+)#i', $ctype, $matches))
            || preg_match('#<meta[^>]+charset=[\'"]?([\w-]+)#i', $this->page, $matches)) {
            $charset = $matches[1];
        }

        if ($charset && strtoupper($charset) !== 'UTF-8') {
            $this->page = iconv($charset, 'UTF-8', $this->page);
        }
        curl_close($ch);
    }

    private function find_links():array
    {
        $template = "#(<a[^>]*href=['\"].*['\"][^<]*>.*</a>)#sUsi";
        preg_match_all($template, $this->page, $matches);
        return $matches[1];
    }

    private function find_images():array
    {
        $result = [];
        $template = "#(<img[^>]*src=['\"])(.*)(['\"][^<]*>)#sUsi";
        preg_match_all($template, $this->page, $matches);
        for ($i = 0; $i < count($matches[2]); $i++) {
            $url_match = "";
            if((strpos($matches[2][$i],"http://") === false || strpos($matches[2][$i],"http://")) != 0 &&
                (strpos($matches[2][$i] ,"https://") === false || strpos($matches[2][$i] ,"https://") != 0))
                if($matches[2][$i][0]!="/")
                    $url_match = $this->url."/".$matches[2][$i];
                else
                    $url_match = $this->url.$matches[2][$i];
            $result[] = $matches[1][$i].$url_match.$matches[3][$i];
        }
        return $result;
    }

    private function find_text():array
    {
        $template = "#.*(<[^<>]*>[^<>]*".$this->text."[^<>]*<\/[^<>]*>).*#";
        preg_match_all($template, mb_strtolower($this->page), $matches);
        return $matches[1];
    }

    private function validate_form(): bool
    {
        if (array_key_exists("url", $_POST) && !empty($_POST["url"])){
            $this->url = $_POST["url"];
            if ($this->url[-1] == "/"){
                $this->url = substr($this->url,0,-1);
            }

            if(strpos($this->url, "www") == false){
                $this->url = str_replace("http://", "http://www.", $this->url);
                $this->url = str_replace("https://", "https://www.", $this->url);
            }

            if((strpos($this->url ,"http://") === false || strpos($this->url ,"http://")) != 0 &&
                (strpos($this->url ,"https://") === false || strpos($this->url ,"https://") != 0))
                $this->url = "https://www.".$this->url;
            try {
                $headers = explode(" " ,@get_headers($this->url)[0]);
                if(count($headers) > 1){
                    $status = (int)explode(" " ,@get_headers($this->url)[0])[1];
                    if(!($status == 200 || $status == 301 || $status == 302))
                        $this->invalid["url"] = $headers;
                }
                else
                    $this->invalid["url"] = "invalid";
            }catch (Exception $e){
                $this->invalid["url"] = "invalid";
            }
        }else
            $this->invalid["url"] = "empty";
        if (array_key_exists("type", $_POST) && $_POST["type"] != null){
            $this->type = (int)$_POST["type"];
            }
        else{
            $this->type = -1;
            $this->invalid["type"] = "empty";
        }


        if (array_key_exists("text", $_POST) && !empty($_POST["text"])){
            $this->text = mb_strtolower($_POST["text"]);
        }elseif($this->type == 2)
            $this->invalid["text"] = "empty";

        return empty($this->invalid);
    }
}