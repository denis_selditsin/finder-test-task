<h3>Результаты</h3>
<div id="accordion" role="tablist" aria-multiselectable="true">
<?php
if (!$data){
    echo "<h6>Ничего не найдено</h6>";
}else
foreach ($data as $item) {
    echo "
    <div class=\"card\">
        <div class=\"card-header\" role=\"tab\" id=\"heading".$item['id']."\">
            <h5 class=\"mb-0\">
                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse".$item['id']."\" aria-expanded=\"false\" aria-controls=\"collapseOne\">
                    ".$item['url']." (".$item['count']." элементов)
                </a>
            </h5>
        </div>

        <div id=\"collapse".$item['id']."\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"heading".$item['id']."\">
            <div class=\"card-block\" style=\"padding: 16px 24px\">
                ".nl2br(htmlspecialchars($item['elements']))."
            </div>
        </div>
    </div>";
}?>
</div>

