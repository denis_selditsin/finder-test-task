<h3>Главная</h3>
<div class="px-5">
    <div class="alert alert-success" role="alert" id="alert-success" style="display: none">
        Поиск успешно выполнен! <a href="/results">Посмотреть результаты</a>
    </div>
    <div class="alert alert-danger" role="alert" id="alert-url-empty" style="display: none">
        Заполните поле Адрес сайта
    </div>
    <div class="alert alert-danger" role="alert" id="alert-url-invalid" style="display: none">
        URL адрес недействителен
    </div>
    <div class="alert alert-danger" role="alert" id="alert-text" style="display: none">
        Заполните поле Текст
    </div>
    <div class="alert alert-danger" role="alert" id="alert-type" style="display: none">
        Тип контента указан неверно
    </div>
    <div class="alert alert-danger" role="alert" id="alert-db" style="display: none">
        Не удалось выполнить запрос к базе данных
    </div>
    <div class="alert alert-warning" role="alert" id="alert-elements-empty" style="display: none">
        Ни один элемент не был найден
    </div>
    <form method="post" id="form-send" action="/find">
        <div class="form-group">
            <label for="url">Адрес сайта</label>
            <input type="text" class="form-control" id="url" name="url" placeholder="Введите адрес сайта с или без http(s)://">
        </div>
        <div class="form-group">
            <label for="type">Тип контента</label>
            <select class="form-control" id="type" name="type">
                <option>Ссылки</option>
                <option>Изображения</option>
                <option>Текст</option>
            </select>
        </div>
        <div class="form-group" id="text-block" style="display: none">
            <label for="text">Текст</label>
            <input type="text" class="form-control" id="text" name="text" placeholder="Введите текст, который желаете найти на странице">
        </div>
        <button id="submit" type="button" class="btn btn-primary">Поиск</button>
        <div class="spinner-border spinner-border-sm text-primary" role="status" id="spinner" style="display: none">
            <span class="sr-only">Loading...</span>
        </div>
    </form>
</div>
<script>
    $("#type").change(function () {
        if (this.options.selectedIndex === 2){
            $("#text-block").show();
        }else{
            $("#text-block").hide();
        }
    });

    $("#submit").click(function () {
        $("#alert-success").hide();
        $("#alert-url-empty").hide();
        $("#alert-text").hide();
        $("#alert-url-invalid").hide();
        $("#alert-type").hide();
        $("#alert-elements-empty").hide();
        $("#alert-db").hide();
        if (validateForm()){
            $("#spinner").show();
            let url_value = $("#url").val();
            let type_value = $("#type")[0].selectedIndex;
            let text_value = $("#text").val();
            $.ajax({
                type: "POST",
                url: "find",
                data: {url: url_value, type: type_value, text: text_value},
                dataType: "json"
            }).done(function( result )
            {
                $("#spinner").hide();
                console.log(result)
                if(result.result === "ok"){
                    $("#alert-success").show();
                }else{
                    if(result.invalid.url === "invalid")
                        $("#alert-url-invalid").show();
                    if(result.invalid.url === "empty")
                        $("#alert-url-empty").show();
                    if(result.invalid.text === "empty")
                        $("#alert-text").show();
                    if(result.invalid.type === "empty" || result.invalid.type === "invalid")
                        $("#alert-type").show();
                    if (result.invalid.db === "invalid")
                        $("#alert-db").show();
                    if (result.invalid.elements === "empty")
                        $("#alert-elements-empty").show();
                }
            });
        }
    });

    function validateForm() {
        let url = $("#url").val();
        let type = $("#type")[0].selectedIndex;
        let text = $("#text").val();

        let invalid = [];

        if (!Boolean(url)){
            $("#alert-url-empty").show();
            let url_index = invalid.indexOf("url");
            if(url_index === -1) {
                invalid.push("url")
            }
        }else{
            $("#alert-url-empty").hide();
            let url_index = invalid.indexOf("url");
            if(url_index > -1){
                invalid.slice(url_index, 1);
            }
        }
        if(type === 2 && !Boolean(text)){
            $("#alert-text").show();
            invalid.push("text")
            let url_index = invalid.indexOf("text");
            if(url_index === -1) {
                invalid.push("text")
            }
        }else{
            $("#alert-text").hide();
            let text_index = invalid.indexOf("text");
            if(text_index > -1){
                invalid.slice(text_index, 1);
            }
        }

        return invalid.length === 0;
    }
</script>
